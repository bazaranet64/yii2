<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

/**
 * Description of SaveComponent
 *
 * @author BuKTOP
 */

use yii\base\Component;

class SaveComponent extends Component{
    public static $data = array();
    
    public function init() {
        parent::init();
    }

    public function design($name, $value){
        if (isset($name) && isset($value))
            return $this->data[$name][] = $value;
        else return false;
        
    }
}
