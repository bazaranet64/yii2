<?php

namespace app\components;

use yii\base\Widget;

/**
 * Description of PostComponent
 *
 * @author BuKTOP
 */
class PostComponent extends Widget {

    protected $id;
    protected $title;
    protected $description;
    protected $icon;
    protected $image;
    protected $count;
    protected $date;
    protected $url;
    protected $bottom;
    
    public function init() { // функция инициализации. Если данные не будут переданы в компонент, то он выведет текст "Текст по умолчанию"
        parent::init();
        $this->title = 'заголовок не задан';
    }
    
    public function __set($name, $value) {
        
        if (property_exists($this, $name)) {
            $this->$setter = $value;
        } else {
            throw new UnknownPropertyException('Setting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function display(){
        return $this->render('post',[
                 'id' => $this->id,
                 'title' => $this->title,
                 'content' => $this->content,
                 'icon' => $this->icon,
                 'image' => $this->image,
                 'count' => $this->count,
                 'time' => $this->time,
                 'url' => $this->url,
                 'bottom' => $this->bottom,
             ]);
    }
    
    public function run() {
        parent::run();
        return $this->display();
    }
}
